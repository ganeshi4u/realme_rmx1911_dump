#!/system/bin/sh
if ! applypatch -c EMMC:/dev/block/bootdevice/by-name/recovery:67108864:fd2a81f37dd14b115b6637eb9795dc83b83b23ed; then
  applypatch  EMMC:/dev/block/bootdevice/by-name/boot:67108864:8f7c5f20e920d5bbb334736ff64c424e9b353d7d EMMC:/dev/block/bootdevice/by-name/recovery fd2a81f37dd14b115b6637eb9795dc83b83b23ed 67108864 8f7c5f20e920d5bbb334736ff64c424e9b353d7d:/system/recovery-from-boot.p && log -t recovery "Installing new recovery image: succeeded" || log -t recovery "Installing new recovery image: failed"
else
  log -t recovery "Recovery image already installed"
fi
